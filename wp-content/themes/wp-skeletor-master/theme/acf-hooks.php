<?php

add_action("gform_post_submission", "acf_post_submission", 10, 2);

function acf_post_submission ($entry, $form)
{
	$post_id = $entry["post_id"];
	if(isset($entry[6])){
	$urlArray = unserialize($entry[6]);
	$urlno =  sizeof($urlArray);
	update_field('acf_repeater_list', $urlno, $post_id);
	
	
	if(sizeof($urlArray) > 1 ){
	
	foreach($urlArray as $key => $url){	
	
	update_field('acf_repeater_list_'.$key.'_url', $url, $post_id);	
	
		} // Foreach End

	
		} // if end
								
		} // isset End
	
	// Attach Images for Gallery code Start

	if(isset($entry[5])){
	$galleryImages = $entry[5];
	
	$trimmed  = trim($galleryImages, '"[');
	$trimmed  = trim($trimmed, ']"');
	$galleryImages  = explode('","', $trimmed);
	

	$gallryAttach = Array();
	
	if(sizeof($galleryImages) > 0){
	foreach($galleryImages as $key => $galleryImage){
	
		
				$relativepath = explode('wp-content',$galleryImage);
				$relativepath =  str_replace("\\","",$relativepath[1]);

				$filename = ABSPATH.'wp-content'.$relativepath;

				$filetype = wp_check_filetype( basename( $filename ), null );

				$wp_upload_dir = wp_upload_dir();

				$attachment = array(
					'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ), 
					'post_mime_type' => $filetype['type'],
					'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
					'post_content'   => '',
					'post_status'    => 'inherit'
				);

				$gallryAttach[]  = wp_insert_attachment( $attachment, $filename );
				require_once( ABSPATH . 'wp-admin/includes/image.php' );

				$attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
				wp_update_attachment_metadata( $attach_id, $attach_data );

	}
	

update_field('gallery', serialize($gallryAttach), $post_id);

/* ******** Show Data After Submission********* */

} // Sizeof Validation Ends

} // Isset Ends
 
 
 /* Show the last result  */

?>




<?php 
$post = get_post($post_id); ?>
<div class="preview">
<div class="preview-inner">


<div class="half-width title-bg">
<h2 class="preview-title">
<?php echo $post->post_title; ?> </h2>
</div>


<div class="half-width">
<label class="one-fifth">Content : </label>
<p class="four-fifth"> <?php echo $post->post_content ; ?></p>
</div>


<div class="half-width">
<label class="one-fifth">Feature Image : </label>
<p class="four-fifth"> <?php $post_thumbnail_id = get_post_thumbnail_id( $post_id ); ?>  
<?php 
$thumb_id = 25;
$url = wp_get_attachment_thumb_url( $post_thumbnail_id );
?>
<img src="<?php echo $url ?>"/>
</p>
</div>
 
 
<div class="half-width gallery">
<label class="one-fifth">Gallery : </label>
<p class="four-fifth">
<ul> <?php $images = get_field( "gallery", $post_id ); ?> 
<?php foreach($images as $imageID){
echo '<li><img src="'.wp_get_attachment_thumb_url($imageID).'"></li>';
}
?>
</ul>
</p>
</div>


<div class="half-width">
<label class="one-fifth">Tags : </label> 
<p class="four-fifth">
<?php $tags = wp_get_post_terms($post_id, 'post_tag', array("fields" => "all"));

foreach($tags as $tag){
echo $tag->name.',';
}
?> 
</p> 
</div>


<div class="half-width">
<label class="one-fifth">URL : </label>
<p class="four-fifth">
<ul>
<?php $number =  get_field( "acf_repeater_list", $post_id ); 
for ($i=0 ; $i < $number & $number < 20; $i++){
echo '<li>'.get_field( "acf_repeater_list_".$i."_url", $post_id ).'</li>';
}
?> 
</ul>
</p>
</div>



<div class="half-width">
<label class="one-fifth">Address : </label>
<p class="four-fifth">
<?php echo get_field( "house_number", $post_id ).','; ?>
<?php echo get_field( "street", $post_id ).','; ?>
<?php echo get_field( "superb", $post_id ).','; ?>
<?php echo get_field( "state", $post_id ).','; ?>
<?php echo get_field( "postcode", $post_id ).','; ?>
<?php echo get_field( "country", $post_id ); ?>
</p>
</div>


</div>
</div>

 
<?php  
} // Hook Function Ends
?>






<?php

/* **** Address Field Special Character Validation Validation */

add_filter( 'gform_field_validation_1_8', 'specialchar_validation', 10, 4 );
add_filter( 'gform_field_validation_1_9', 'specialchar_validation', 10, 4 );
add_filter( 'gform_field_validation_1_10', 'specialchar_validation', 10, 4 );
add_filter( 'gform_field_validation_1_11', 'specialchar_validation', 10, 4 );
add_filter( 'gform_field_validation_1_12', 'specialchar_validation', 10, 4 );
add_filter( 'gform_field_validation_1_13', 'specialchar_validation', 10, 4 );


function specialchar_validation( $result, $value, $form, $field ) {

    if ( $result['is_valid'] && preg_match('/[\'^£$%&*()}{@#~?><>,!|=_+¬-]/', $value)) {
        $result['is_valid'] = false;
        $result['message'] = 'Special Character are not allowed in Address';
    }
    return $result;
} 