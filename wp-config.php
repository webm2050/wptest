<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wpskeltor');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '1f6#CP7$*z]+l/0qp4=#{0Jeg|[u*Gu}+Okmtk:-/8M6)8]!6oOt0,PBTGAPV>L*');
define('SECURE_AUTH_KEY',  '{kA;sJ-f6-rZqlCa%a:l>n%JYWBL-$Qt,<|TNybT#h|@5WLKfEDX+}3;$Q( /&/$');
define('LOGGED_IN_KEY',    'AP&t~#`#>B=dk>V*9zzMSB(R?6FI?9BOgciY|;$c96dSU%*gb_~.P~2r%tyG%;$y');
define('NONCE_KEY',        'U.wjNiJ1G#grOIO#3BkQ,tWjgPzPP|Sy]Sh]Uy`[SPOdEOt6XKG>[XG~l?K+%WAo');
define('AUTH_SALT',        'V`9vx<0P<yl} DFoJTDy<[fmQX:99lF~xoOV6pw>]m?2A./&HO`Af+%q8g|7RXu]');
define('SECURE_AUTH_SALT', '-mNLV[|Qrg aR%D-s1}zuG1Di|!TS9tuiU(e<Wz^Dv!~~(rx$UU|-A@eG^C|{.L!');
define('LOGGED_IN_SALT',   'tf{yjo)ed+f*M[&re-G-]XDwR0E_Z|Z4#BRoKHqx;0(kepqy/<Z_q:Bj LnZDv^L');
define('NONCE_SALT',       '`|,{#+9n9C|L|H?N5Tam5|/zGm-{3nNQDci|KSD)%| }35Q;E/G8Oe,95He%g1ok');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
